<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>PROJET MVC</title>
        <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
        <link rel="stylesheet" href="css/style.css" type="text/css">
        
        <script src="js/jquery-3.3.1.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
    </head>
    <body>
        
    <?php
    include PATHVIEWS.'navbar.php';
    ?>        
        <!-- Fin header -->