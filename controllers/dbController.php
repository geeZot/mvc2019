<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of dbController
 *
 * @author fabrice
 */
class dbController extends configController {
    private $bddserver = '127.0.0.1';
    private $bddname = ''; 
    private $bdduser = 'root';
    private $bddpassword = '';
    private $bdddriver = '';
    private $bddlink;

    /* @TODO Faire correspondre les champs aux colonnes de la bdd
     * @TODO Centraliser la gestion des erreurs et des exceptions
     * @TODO Optimiser la récupération de l'objet
     */
    
    function __construct() {
        parent::__construct();
        $config = parent::getConfigParameter('dbConfig');
        
        foreach($config as $key=>$value){
            $method = 'set'.ucfirst($key);
            if(method_exists($this, $method)){
                $this->$method($value);   
            }
        }
        
        $dsn = $this->bdddriver.':dbname='.$this->bddname.';host='.$this->bddserver;
        
        try {
            $this->bddlink = new PDO($dsn , $this->bdduser, $this->bddpassword);
            $this->bddlink->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
        } catch (PDOException $e) {
            logController::createLog($e->getMessage(), 'error');
            //echo 'Connection failed: ' . $e->getMessage();
        }
    }


    function getBddserver() {
        return $this->bddserver;
    }

    function getBddname() {
        return $this->bddname;
    }

    function getBdduser() {
        return $this->bdduser;
    }

    function getBddpassword() {
        return $this->bddpassword;
    }
    
    function getBdddriver() {
        return $this->bdddriver;
    }

    function getBddlink() {
        return $this->bddlink;
    }

    function setBddlink($bddlink) {
        $this->bddlink = $bddlink;
    }

        
    function setBdddriver($bdddriver) {
        $this->bdddriver = $bdddriver;
    }

    function setBddserver($bddserver) {
        $this->bddserver = $bddserver;
    }

    function setBddname($bddname) {
        $this->bddname = $bddname;
    }

    function setBdduser($bdduser) {
        $this->bdduser = $bdduser;
    }

    function setBddpassword($bddpassword) {
        $this->bddpassword = $bddpassword;
    }

    /*
     * @parameters $option = array(champs,critere => valeurs
     *      */
    function findOneBy(object $objet, array $options=array()) {
        try {
            $table = get_class($objet);
            $champs = '*';
            if(isset($options['champs']) && !empty($options['champs'])) {
                $champs = implode(',',$options['champs']);
            }

            if(!isset($options['criteria'])){
                throw new Exception(__METHOD__.' '.__LINE__.' : criteria doit être défini !');
            }

            $query = 'SELECT '.$champs.' FROM '.$table.' WHERE ';
            $nbCriteria = count(array_keys($options['criteria']));
            $keys = array_keys($options['criteria']);
            
            for($i=0; $i < $nbCriteria; $i++ ) {
                if($i > 0 ) {
                    $query .= ' AND ';
                }
                $query .= $keys[$i].' = :'.$keys[$i];
            } 
            
            $query .= ' LIMIT 1';
            
            $req = $this->bddlink->prepare($query);
            $req->execute($options['criteria']);

            $result = $req->fetch(PDO::FETCH_ASSOC);
            return $result;
        } catch (Exception $ex) {
            echo $ex->getMessage();
            return array();
        }
        
    }
    
    function findOneById(object $objet,$id) {
        return $this->findOneBy($objet, 
                array(
                    'criteria' => array('id'=>$id)
                    ));
        //Penser à retourner un objet ;)
    }
    
    function findObjectById(object $object,$id) {
        $datas = $this->findOneById($object,$id);
        $this->hydrateRecord($object, $datas);
    }
    
    function findAll(object $object) {
        try {
            $table = get_class($object);
            $query = 'SELECT * FROM '.$table;
            
            $req = $this->bddlink->prepare($query);
            $req->execute();

            $result = $req->fetchAll(PDO::FETCH_CLASS,$table);
            return $result;
            
        } catch (Exception $ex) {
            echo $ex->getMessage();
            return 0;
        }
    }


    function hydrateRecord(object $object, array $datas) {
        foreach ($datas as $key => $value) {
            $method = 'set'.ucfirst($key);
            if(method_exists($object, $method)){
                $object->$method($value);   
            }
        }
    }
    
    function newRecord(object $object, array $datas=array()) {
        try {
            
            if(empty($datas)){
                throw new Exception(__METHOD__.' '.__LINE__.' : datas ne peut être vide');
            }
            $table = get_class($object);
        
            $rowColumns = '`'.implode('`,`',  array_keys($datas)).'`';
            $rowValues = ':'.implode(',:', array_keys($datas));

            $reqNewRecord = 'INSERT INTO '.$table.' ('.$rowColumns.') VALUES ';
            $reqNewRecord .= '('.$rowValues.')';

            //echo $reqNewRecord; die();

            $Enregistrement = $this->bddlink->prepare($reqNewRecord);
            $Enregistrement->execute($datas);

            return $this->bddlink->lastInsertId();
        } catch (Exception $ex) {
            echo $ex->getMessage();
            return 0;
        }
    }
    
    public function updateRecord(object $object, array $datas=array()) {
        try{
            if(empty($datas)){
                throw new Exception(__METHOD__.' '.__LINE__.' : datas ne peut être vide');
            }
            $table = get_class($object);
            
            //Affectation des valeurs du formulaire à ma requête
            $reqUpdateRecord = 'UPDATE '.$table.' SET ';

            $keys = array_keys($datas);
            $nbKeys = count($keys);
            
            for($i=0; $i < $nbKeys; $i++ ) {
                if($i > 0 ) {
                    $reqUpdateRecord .= ', ';
                }
                $reqUpdateRecord .= $keys[$i].' = :'.$keys[$i];
            }

            $reqUpdateRecord .= ' WHERE id = :id';

            $datas['id'] = $object->getId();

            //Préparation de la requête
            $resUpdateRecord = $this->bddlink->prepare($reqUpdateRecord);

            $resUpdateRecord->execute($datas);

            return $resUpdateRecord->rowCount();
        } catch (Exception $ex) {
            echo $ex->getMessage();
            return 0;
        }
                
    }
    
    public function deleteRecord(object $object) {
        try{
            if(empty($object)){
                throw new Exception(__METHOD__.' '.__LINE__.' : object ne peut être vide');
            }
            $table = get_class($object);
            
            //Affectation des valeurs du formulaire à ma requête
            $reqDeleteRecord = 'DELETE FROM '.$table.' WHERE id = :id ';
            
            $datas['id'] = $object->getId();

            //Préparation de la requête
            $resDeleteRecord = $this->bddlink->prepare($reqDeleteRecord);

            $resDeleteRecord->execute($datas);

            return $resDeleteRecord->rowCount();
            
        } catch (Exception $ex) {
            echo $ex->getMessage();
            return 0;
        }
    }

}
