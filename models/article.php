<?php

/**
 * Description of article
 *
 * @author fabrice
 */
class article {
    private $id;
    private $contenu;
    private $created;
    private $titre;
    private $image;
    private $user_id;
    
    function getId() {
        return $this->id;
    }

    function getContenu() {
        return $this->contenu;
    }

    function getCreated() {
        return $this->created;
    }

    function getTitre() {
        return $this->titre;
    }

    function getUser_id() {
        return $this->user_id;
    }

    function getImage() {
        return $this->image;
    }

    function setImage($image) {
        $this->image = $image;
    }

    function setId($id) {
        $this->id = $id;
    }

    function setContenu($contenu) {
        $this->contenu = $contenu;
    }

    function setCreated($created) {
        $this->created = $created;
    }

    function setTitre($titre) {
        $this->titre = $titre;
    }

    function setUser_id($user_id) {
        $this->user_id = $user_id;
    }


}
