<?php

/**
 * Description of articleController
 *
 * @author fabrice
 */
class articleController {
    
    function createAction() {
        
        $article = new article();
        $oBdd = new dbController();
        
        $articlePost = array(
                'titre' => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
                'contenu' => FILTER_SANITIZE_FULL_SPECIAL_CHARS
                );

        $articleTab = filter_input_array(INPUT_POST,$articlePost);
        $articleTab['user_id'] = $_SESSION['userid'];
        
        $id = $oBdd->newRecord($article, $articleTab);
        
        if($id === 0) {
            $_SESSION['msgStyle'] = 'danger';
            $_SESSION['msgTxt'] = 'Erreur lors de la création de l\'article';
            return 0;
        }
        
        if($_FILES['image']['size'] > 0){
            $image_name = $this->addPicture();
            $article->setId($id);
            $oBdd->updateRecord($article, array('image'=>$image_name));
        }
        
        $_SESSION['msgStyle'] = 'success';
        $_SESSION['msgTxt'] = 'l\'article a correctement été créé';
        
        logController::createLog('Article '.$id.' créé');
        
        return $id;
    }
    
    function listeAction() {
        $article = new article();
        $oBdd = new dbController();
        
        $collectionArticle = $oBdd->findAll($article);
        
        if($collectionArticle === 0) {
            $_SESSION['msgStyle'] = 'danger';
            $_SESSION['msgTxt'] = 'Aucun article à afficher';
            return 0;
        }
        return array('view'=>'listearticle', 'articles'=>$collectionArticle); 
        
    }
    
    function editAction(){
        // Récupère les ID dans les urls
        $id = filter_input(INPUT_GET,'id', FILTER_SANITIZE_NUMBER_INT);
        try {
            if(is_null($id)){
                throw new Exception(__METHOD__.' '.__LINE__.' : id ne doit pas être null !');
            }
            
            $oBdd = new dbController();
            $article = new article(); 
            
            $oBdd->findObjectById($article, $id);

            return array('view'=>'editarticle','article' => $article);
        } catch (Exception $ex) {
            echo $ex->getMessage();
            return 0;
        }
    }
    
    function updateAction() {
        $article= new article();
        $oBdd = new dbController();
        
        $articlePost = array(
                'titre' => FILTER_SANITIZE_FULL_SPECIAL_CHARS,
                'contenu' => FILTER_SANITIZE_FULL_SPECIAL_CHARS
                );
        $id = filter_input(INPUT_GET,'id', FILTER_SANITIZE_NUMBER_INT);
        
        $articleTab = filter_input_array(INPUT_POST,$articlePost);
        
        $article->setId($id);
        
        $nbUpdate = $oBdd->updateRecord($article, $articleTab);
        
        if($nbUpdate === 0) {
            $_SESSION['msgStyle'] = 'danger';
            $_SESSION['msgTxt'] = 'Erreur lors de la modification de l\'article';
            return 0;
        }
        
        if($_FILES['image']['size'] > 0){
            $image_name = $this->addPicture();
            $oBdd->updateRecord($article, array('image'=>$image_name));
        }
        
        $_SESSION['msgStyle'] = 'success';
        $_SESSION['msgTxt'] = 'Article correctement modifié';
        header('location:?action=article-liste');
        
    }
    
    function deleteAction() {
        $oBdd = new dbController();
        $article = new article();
        $id = filter_input(INPUT_GET,'id', FILTER_SANITIZE_NUMBER_INT);
        
        try {
            if(is_null($id)){
                throw new Exception(__METHOD__.' '.__LINE__.' : id ne doit pas être null !');
            }
            
            $article->setId($id);
        
            $nbDelete = $oBdd->deleteRecord($article);

            if($nbDelete === 0) {
                $_SESSION['msgStyle'] = 'danger';
                $_SESSION['msgTxt'] = 'Erreur lors de la suppression de l\'article';
                return 0;
            }
            $_SESSION['msgStyle'] = 'success';
            $_SESSION['msgTxt'] = 'Article correctement supprimé';

            header('location:?action=article-liste');
            
        } catch (Exception $ex) {
            echo $ex->getMessage();
            return 0;
        }            
        
    }
    
    function addPicture() {
        $image = $_FILES['image'];
        $imagename = null;
        if($image['error']===0) {
            // chown -R :www-data upload
            $imagename=(move_uploaded_file($image['tmp_name'], PATHUPL.$image['name']))?$image['name']:null;
        }
        
        return $imagename;
    }
    
    function gallerieAction() {
        $article = new article();
        $oBdd = new dbController();
        
        $collectionArticle = $oBdd->findAll($article);
        
        return array('view'=>'gallerie', 'articles'=>$collectionArticle);
    }
        
}
