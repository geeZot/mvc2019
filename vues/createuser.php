<?php
if($page === 'edituser') {
    $title = 'Modifier mon compte';
    $action = 'user-update';
    $bouton = 'Modifier';
    $delete = '<a href="?action=user-delete" class="alert alert-danger">x</a>';
    $login = $resAction['user']->getLogin();
} else {
    $title = 'Nouveau compte';
    $action = 'user-create';
    $bouton = 'Créer';
    $delete = '';
    $login='';
}
?>
<h1><?=$title;?> <?=$delete;?></h1>
<form method="POST" action="?action=<?=$action;?>" class="col-6">
    <input type="email" name="login" placeholder="Entrez votre login" class="form-control" value="<?= $login;?>">
    <input type="password" name="password" placeholder="Entrez votre mot de passe" class="form-control" value="">
    <input type="submit" value="<?=$bouton;?>" class="btn btn-primary">
</form>


