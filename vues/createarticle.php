<?php
if($page === 'editarticle') {
    $title = 'Modifier article';
    $bouton = 'Modifier';
    $article = $resAction['article'];
    $action = 'article-update&id='.$article->getId().'';
    $delete = '<a href="?action=article-delete&id='.$article->getId().'" class="alert alert-danger">x</a>';
    
} else {
    $title = 'Nouvel article';
    $action = 'article-create';
    $bouton = 'Créer';
    $delete = '';
    $article = '';
}
?>

<h2><?=$title;?></h2>
<form method="POST" action="?action=<?=$action;?>" enctype="multipart/form-data">
    <?php
    if(!empty($article)) {
        if (!is_null($article->getImage())) {
        ?>
    <img src="upload/<?=$article->getImage();?>" width="200px">
        <?php
        }
    }
    ?>
    <p>
        <input type="text" name="titre" placeholder="titre" 
               value="<?=(!empty($article))?$article->getTitre():'';?>">
    </p>
    <p>
        <textarea name="contenu"><?=(!empty($article))?$article->getContenu():'';?></textarea>
    </p>
    <p><input type="file" name="image"></p>
    
    <p><input type="submit"value="<?=$bouton;?>"></p>
</form>