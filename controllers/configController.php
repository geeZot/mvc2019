<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of configController
 *
 * @author fabrice
 */
abstract class configController {
    
    var $config;
    
    public function __construct() {
        // Récupération du fichier de configuration
        $this->config = yaml_parse_file(PATHROOT.DS.'conf'.DS.'parameters.yml');
    }
    
    function getConfig() {
        return $this->config;
    }

    function setConfig($config) {
        $this->config = $config;
    }

    function getConfigParameter($parameter){
        
        if(key_exists($parameter, $this->config)){
            return $this->config[$parameter];
        }
        
    }
    
}
